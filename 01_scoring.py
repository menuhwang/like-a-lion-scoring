import requests
from libs.GoogleSpreadsheetUtil import GoogleSpreadsheet
from libs.ProjectScore import ProjectScore


if __name__ == '__main__':
    sheet_url = "https://docs.google.com/spreadsheets/d/1cJ9XQDISo3B6UHzcDmWtG9ucDRDg_YgJPkkW9atCFjk"
    sheet_name = "종합프로젝트"
    test_user_name = "kyeongrok37"
    gs = GoogleSpreadsheet(sheet_url, sheet_name)
    gs.update_student_list('A4:C93')
    infos = gs.read_saved_csv()

    scores = {}
    cicd_test_passed = []
    l = enumerate(infos)
    for i,info in list(l):
        if info[2] != "\n":
            # print('idx:', i)
            url = info[2]
            name = info[1]
            ps = ProjectScore(test_user_name)
            score = ps.doScore(name, url)
            scores[name] = score
            # gs.write_score(sheet_name,i+4,4,score)
            if score >= 4 : cicd_test_passed.append(name)


    print(len(cicd_test_passed), cicd_test_passed)



