import requests

from report.Report import report
from libs.ProjectScore import ProjectScore

# 혼자 채점용
# 사용방법 아래 url, name에 본인의 api url과 이름을 넣고 실행 하세요.
if __name__ == '__main__':
    test_user_name = "kyeongrok35"

    url = "http://ec2-43-200-183-93.ap-northeast-2.compute.amazonaws.com:8080"
    name = "황민우"
    try:
        ps = ProjectScore(test_user_name, name)
        ps.doScore(name, url)

    except requests.exceptions.Timeout:
        print(name, "Timeout error:")
    except Exception as e:
        print(name, f"error:{e}", url)

    report(ps.get_html_table())

