import gspread, os, re
import requests, json


class GoogleSpreadsheet:
    gc = gspread.service_account(filename=os.getenv("GS_KEY_PATH"))
    sheet_url = ''
    workbook = None

    def __init__(self, sheet_url, sheet_name):
        if sheet_url == None:
            print(f'sheet_url을 입력 해주세요.')
            exit(0)
        # 불러오고싶은 sheet의 url을 입력 합니다.
        self.sheet_url = sheet_url
        self.sheet_name = sheet_name


    def read(self, range):
        '''
        시트에 있는 내용을 읽어옵니다.
        :param sheet_name: 시트명
        :param range: 불러올 범위
        '''
        if self.workbook == None:
            self.workbook = self.gc.open_by_url(self.sheet_url)

        sh = self.workbook.worksheet(self.sheet_name)
        # sh = gc.open("명렬표").worksheet("repositories")
        return sh.get(range)

    def read_saved_csv(self, filename="student_list.csv"):
        f = open(filename, encoding="utf-8")
        r = []
        for line in f.readlines():
            r.append(line.split(","))
        return r

    def crawl(self, base_url, params={"since": "2022-10-27T10:50:50Z"}):
        # git repo주소로 crawl해서
        url = base_url + "/commits"
        res = requests.get(url, params=params)
        print(res, url)
        return res

    def student_list_save_to_csv(self, arr):
        # google spreadsheet에서 받은 arr을 csv로 convert한 후 파일로 저장
        # gs api call을 줄이기 위함
        file = open("student_list.csv", "w+", encoding="utf-8")
        for row in arr:
            file.write(f'{row}\n')

    def update_student_list(self, range='A2:G88'):
        '''
        :param range: 'A2:G88'의 String형식
        :return: void
        '''
        repositories_arr = self.read(range)

        # replace단계
        pattern = "(http://.*):(\d*)?"
        for row in repositories_arr:
            if len(row) >=3 :
                try:
                    row[2] = re.search(pattern, row[2]).group(0)
                except:
                    print(row)

        # csv형태로 변경
        arr = []
        for row in repositories_arr:
            s = ""
            for item in row:
                item = item.replace('.git', "")
                s += f'{item},'
            arr.append(s)
        self.student_list_save_to_csv(arr)
        print(f"google spreadsheet {self.sheet_name} 을 참조하여 student_list.csv를 업데이트 했습니다.")

    def write_score(self, sheet_name, row_num, col_num, score):
        sh = self.workbook.worksheet(sheet_name)
        sh.update_cell(row_num, col_num, score)
