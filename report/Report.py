def report(result):
    report_file = open("index.html", "w", encoding="utf-8")
    report_file.write("<html>")
    report_file.write("""
<head>
    <meta charset="UTF-8">
</head>
""")
    report_file.write("""
<style>
    h2 {
        margin: 0px 10px;
    }
    th, td {
      padding: 10px;
      text-align: left;
    }
</style>
    """)
    report_file.write("<body>")
    report_file.write(result)
    report_file.write("</body>")
    report_file.write("</html>")
    report_file.close()
